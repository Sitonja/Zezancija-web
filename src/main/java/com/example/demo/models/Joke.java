package com.example.demo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Formula;




@Entity
public class Joke {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private long id;

	    @NotNull
	    @ManyToOne
	    private Category category;

	    @NotNull
	    @Size(min=1)
	    private String content;
	    
	    @NotNull
	    private int likes;
	    
	    @NotNull
	    private int dislikes;

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public Category getCategory() {
			return category;
		}

		public void setCategory(Category category) {
			this.category = category;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public int getLikes() {
			return likes;
		}

		public void setLikes(int likes) {
			this.likes = likes;
		}

		public int getDislikes() {
			return dislikes;
		}

		public void setDislikes(int dislikes) {
			this.dislikes = dislikes;
		}
		
		@Formula("likes - dislikes")
		private int difference;

		public int getDifference() {
			return difference;
		}

		public void setDifference(int difference) {
	    	this.difference = difference;
	    }
	    
}
