package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.example.demo.models.Joke;
import com.example.demo.repositories.JokeRepository;

@Component
public class JokeService implements IJokeService{
	private JokeRepository jokeRepository;
	
	@Autowired
	public void setJokeRepository(JokeRepository jokeRepository) {
		this.jokeRepository = jokeRepository;
	}
	
	@Override
	public Iterable<Joke> listAllJokes() {
		return jokeRepository.findAll();
	}

	@Override
	public Joke saveJoke(Joke joke) {
		return jokeRepository.save(joke);
	}

	@Override
	public void addLike(Long id) {
		Joke joke = jokeRepository.findOne(id);
		joke.setLikes(joke.getLikes()+1);
		jokeRepository.save(joke);
	}

	@Override
	public void addDislike(Long id) {
		Joke joke = jokeRepository.findOne(id);
		joke.setDislikes(joke.getDislikes()+1);
		jokeRepository.save(joke);
	}
	
	@Override
	public Page<Joke> findAllPageable(Pageable pageable) {
		return jokeRepository.findAll(pageable);
	}
	
	

}
