package com.example.demo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.models.Joke;

public interface IJokeService {
	 Iterable<Joke> listAllJokes();
	 
	 Joke saveJoke(Joke joke);	
	 
	 void addLike(Long id);
	 
	 void addDislike(Long id);
	 
	 Page<Joke> findAllPageable(Pageable pageable);
}
