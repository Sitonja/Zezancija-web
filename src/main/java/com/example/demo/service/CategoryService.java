package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.models.Category;
import com.example.demo.repositories.CategoryRepository;

@Component
public class CategoryService implements ICategoryService{

private CategoryRepository categoryRepository;
	
	@Autowired
	public void setJokeRepository(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}
	
	@Override
	public Iterable<Category> listAllJokes() {
		return categoryRepository.findAll();
	}

}
