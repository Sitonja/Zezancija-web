package com.example.demo.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.example.demo.models.Joke;

public interface JokeRepository extends PagingAndSortingRepository<Joke, Long>{
}
