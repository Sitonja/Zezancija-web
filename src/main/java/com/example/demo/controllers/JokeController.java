package com.example.demo.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.helpers.Pager;
import com.example.demo.models.Joke;
import com.example.demo.service.CategoryService;
import com.example.demo.service.JokeService;


@Controller
public class JokeController {
	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	
	
	@Autowired
	private JokeService jokeService;
	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping("/")
	public String index(Model model, @RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page) {
		
		// Evaluate page size. If requested parameter is null, return initial
		// page size
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		// Evaluate page. If requested parameter is null or less than 0 (to
		// prevent exception), return initial size. Otherwise, return value of
		// param. decreased by 1.
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		Page<Joke> jokes = jokeService.findAllPageable(new PageRequest(evalPage, evalPageSize, Sort.Direction.DESC, "difference"));
		Pager pager = new Pager(jokes.getTotalPages(), jokes.getNumber(), BUTTONS_TO_SHOW);

		model.addAttribute("jokes", jokes);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		return "index";
	}
	
	@PostMapping("/addJoke")
	public String addProductType(@Valid Joke joke, BindingResult bindingResult, Model model){
		
		  if (bindingResult.hasErrors()) {
			  	
				model.addAttribute("allCategories", categoryService.listAllJokes());
	            return "newJoke";
        }
		jokeService.saveJoke(joke);
		return "redirect:/";
	}
	
	@RequestMapping(value = "/addLike", method = RequestMethod.POST)
    @ResponseBody
    public String addLike(@RequestParam Long Id){
        jokeService.addLike(Id);
        return Id.toString();
    }
	
	@RequestMapping(value = "/addDislike", method = RequestMethod.POST)
    @ResponseBody
    public String addDislike(@RequestParam Long Id){
        jokeService.addDislike(Id);
        return Id.toString();
    }
	
	@RequestMapping("/new")
	public String newJoke(Model model) {
		model.addAttribute("joke",new Joke());
		model.addAttribute("allCategories", categoryService.listAllJokes());
		return "newJoke";
	}
	
	
}
