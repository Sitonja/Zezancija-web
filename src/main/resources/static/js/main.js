$(document).ready(function () {
	changePageAndSize();
	
    $(".like-button").click(function (event) {
        //stop submit the form, we will post it manually.
    	event.preventDefault();
        fire_ajax_like_submit($(this));
    });
    
    $(".dislike-button").click(function (event) {
        //stop submit the form, we will post it manually.
    	event.preventDefault();
        fire_ajax_dislike_submit($(this));
    });

});

function changePageAndSize() {
	$('#pageSizeSelect').change(function(evt) {
		window.location.replace("/?pageSize=" + this.value + "&page=1");
	});
}

function fire_ajax_like_submit(buttonClicked) {
	var Id = buttonClicked.val();
    $.ajax({
        type: "POST",
        url: "/addLike",
        data:{Id:Id} ,
        success: function (data) {

        	likesRow = buttonClicked.closest('tr').children('td.likes');
            differenceRow = buttonClicked.closest('tr').children('td.difference');
            
        	likesRow.text(parseInt(likesRow.text())+1);
        	differenceRow.text(parseInt(differenceRow.text())+1);
        	
        	console.log("SUCCESS : ", likesRow.text());

        },
        error: function (e) {
        	console.log("FAIL : ", e);
        }
    });
}

function fire_ajax_dislike_submit(buttonClicked) {
	var Id = buttonClicked.val();
    $.ajax({
        type: "POST",
        url: "/addDislike",
        data:{Id:Id} ,
        success: function (data) {

        	dislikesRow = buttonClicked.closest('tr').children('td.dislikes');
            differenceRow = buttonClicked.closest('tr').children('td.difference');
            
        	dislikesRow.text(parseInt(dislikesRow.text())+1);
        	differenceRow.text(parseInt(differenceRow.text())-1);
        	
        	console.log("SUCCESS : ", dislikesRow.text());

        },
        error: function (e) {
        	console.log("FAIL : ", e);
        }
    });
}