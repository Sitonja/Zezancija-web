create table category (
  id bigserial not null,
  name text not null,
  primary key (id)  
);

create table joke (
  id bigserial not null,
  content text not null,
  likes integer not null DEFAULT 0 CHECK (likes >= 0),
  dislikes integer not null DEFAULT 0 CHECK (dislikes >= 0),
  category_id bigserial not null references category (id),
  primary key (id)
);
