INSERT INTO category (name) VALUES
  ('Chuck Norris'),
  ('Mujo'),
  ('Škola');

INSERT INTO joke (content, category_id) VALUES
  (E'Zašto je Chuck Norris najjači? \n
Zato što vježba dva dana dnevno', 1),
  ('Pita nastavnica hrvatskog jezika mladog osnovnoškolca:
Reci ti meni što su to prilozi?
Prilozi su: ketchup, majoneza, luk, salata...', 3),
  ('Pričaju dvije gimnazijalke:
Nema mi roditelja doma ovaj vikend!
Bože, pa koja si ti sretnica! Možeš učiti naglas!', 3),
  ('Došao Mujo u pizzeriju i naručio pizzu. Konobar ga upita:
Želite da vam izrežem pizzu na 6 ili 12 komada?
Ma na 6 komada, nema šanse da pojedem 12.', 2);